function myCallback(element, index, array) {
    console.log("a[" + index + "] = " + element);
}

function myCallback2(value) {
    return `${value} é muito bom`;
}

function isBiggerThan10(element, index, array) {
    return element > 10;
}

const reducer = (accumulator, currentValue) => accumulator + currentValue;

let myArray = [2, 4, 6, 8];
let myArray2 = [8, 5, 12, 1, 4];

function newForEach(array, callback) {
    
    let arr = [];

    for (let i = 0; i < array.length; i++) {
        arr.push(callback(array[i], i, array));
    }

    return undefined;
}

myArray.forEach(myCallback);
newForEach(myArray, myCallback);

function newFill(array, callback) {
    
}

function newMap(array, callback) {

    let arr = [];

    for (let i = 0; i < array.length; i++) {
        arr.push(callback(array[i]));
    }

    return arr;
}

let retornoMap = myArray.map(myCallback2);
let retornoNewMap = newMap(myArray, myCallback2);

console.log(retornoMap);
console.log(retornoNewMap);

function newSome(array, callback) {
    
    let condition = false;

    for (let i = 0; i < array.length; i++) {
        if (callback(array[i], i, array)) {
            condition = true;
        }
    }

    return condition;
}

console.log(myArray.some(isBiggerThan10));
console.log(myArray2.some(isBiggerThan10));
console.log(newSome(myArray, isBiggerThan10));
console.log(newSome(myArray2, isBiggerThan10));

function newFind(array, callback) {
    
    for (let i = 0; i < array.length; i++) {
        if(callback(array[i], i, array)) {
            return array[i];
        }
    }

    return undefined;
}

const found = myArray2.find(element => element > 10);
const newFound = newFind(myArray2, element => element > 10);

console.log(found);
console.log(newFound);

function newFindIndex(array, callback) {

    for (let i = 0; i < array.length; i++) {
        if(callback(array[i], i, array)) {
            return i;
        }
    }

    return -1;
}

const indexFound = myArray2.findIndex(element => element > 10);
const indexNewFound = newFindIndex(myArray2, element => element > 10);

console.log(indexFound);
console.log(indexNewFound);

function newEvery(array, callback) {
    
    let condition = true;

    for (let i = 0; i < array.length; i++) {
        if (!callback(array[i], i, array)) {
            condition = false;
        }
    }

    return condition;
}

console.log([12, 54, 18, 130, 44].every(elem => elem >= 10));
console.log([12, 54, 8, 130, 44].every(elem => elem >= 10));
console.log(newEvery([12, 54, 18, 130, 44], elem => elem >= 10));
console.log(newEvery([12, 54, 8, 130, 44], elem => elem >= 10));

function newFilter(array, callback) {
    
    let arr = [];

    for (let i = 0; i < array.length; i++) {
        if(callback(array[i], i, array)) {
            arr.push(array[i]);
        }
    }

    return arr;
}

var filtered = [12, 5, 8, 130, 44].filter(elem => elem >= 10);
var newFiltered = newFilter([12, 5, 8, 130, 44], elem => elem >= 10);

console.log(filtered);
console.log(newFiltered);

function newConcat(array, callback) {
    
    let arr = [];

    for (let i = 0; i < array.length; i++) {
        arr.push(array[i]);
    }

    for (let i = 0; i < callback.length; i++) {
        arr.push(callback[i]);
    }

    return arr;
}

console.log(myArray.concat(myArray2));
console.log(newConcat(myArray, myArray2));

function newIncludes(array, callback, index = 0) {

    let condition = false;
    
    for (let i = index; i < array.length; i++) {
        if (array[i] === callback) {
            condition = true;
        }
    }

    return condition;
}

console.log([1, 2, 3].includes(2));
console.log([1, 2, 3].includes(3, 3));
console.log(newIncludes([1, 2, 3], 2));
console.log(newIncludes([1, 2, 3], 3, 3));


function newIndexOf(array, callback, pontoInicial = 0) {
    
    let index = pontoInicial;

    if (pontoInicial < 0) {
        index = array.length - (pontoInicial * (-1));
    }

    for (let i = index; i < array.length; i++) {
        if (array[i] === callback) {
            return i;
        }
    }

    return -1;
}

console.log([2, 5, 9].indexOf(2));
console.log([2, 5, 9].indexOf(7));
console.log([2, 5, 9].indexOf(2, -3));
console.log(newIndexOf([2, 5, 9], 2));
console.log(newIndexOf([2, 5, 9], 7));
console.log(newIndexOf([2, 5, 9], 2, -3));

function newJoin(array, callback = ',') {
    
    let str = '';

    for (let i = 0; i < array.length; i++) {
        if (i === array.length -1) {
            str += array[i];
        } else {
            str += array[i] + callback;
        }
    }

    return str;

}

console.log(['Fire', 'Air', 'Water'].join());
console.log(['Fire', 'Air', 'Water'].join(''));
console.log(['Fire', 'Air', 'Water'].join('-'));
console.log(newJoin(['Fire', 'Air', 'Water']));
console.log(newJoin(['Fire', 'Air', 'Water'], ''));
console.log(newJoin(['Fire', 'Air', 'Water'], '-'));

function newReduce(array, callback, accumulator = 0) {

    for (let i = 0; i < array.length; i++) {
        accumulator = callback(accumulator, array[i], i, array);
    }

    return accumulator;
}

console.log([1, 2, 3, 4].reduce(reducer));
console.log([1, 2, 3, 4].reduce(reducer, 5));
console.log(newReduce([1, 2, 3, 4], reducer));
console.log(newReduce([1, 2, 3, 4], reducer, 5));

function newSlice(array, inicio = 0, fim = 0) {
    
    if (fim === 0 || fim > array.length) {
        fim = array.length;
    }

    if (inicio < 0) {
        inicio = array.length + inicio;
    }

    if (fim < 0) {
        fim = array.length + fim;
    }

    let arr = [];

    for (let i = inicio; i < fim; i++) {
        arr.push(array[i]);
    }

    return arr;
}

console.log([1, 2, 3, 4, 5, 6, 7, 8, 9, 10].slice());
console.log([1, 2, 3, 4, 5, 6, 7, 8, 9, 10].slice(3,7));
console.log([1, 2, 3, 4, 5, 6, 7, 8, 9, 10].slice(-2));
console.log([1, 2, 3, 4, 5, 6, 7, 8, 9, 10].slice(3,-1));
console.log(newSlice([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));
console.log(newSlice([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3, 7));
console.log(newSlice([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], -2));
console.log(newSlice([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3, -1));

function newFlat(array, depth) {
    
}

function newFlatMap(array, callback) {
    
}

function newArrayOf() {
    
    var args = [].slice.call(arguments, 0);

    let arr = [];

    for (let i = 0; i < args.length; i++) {
        arr.push(args[i]);
    }

    return arr;
}

console.log(Array.of(7));
console.log(Array.of(1, 2, 3));
console.log(newArrayOf(7));
console.log(newArrayOf(1, 2, 3));